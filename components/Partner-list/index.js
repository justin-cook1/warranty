import $ from 'jquery'
import {orderBy} from 'lodash'

export default class PartnerListing {
  constructor ($element) {
    if (isNotCompatibleElement($element)) return
    this.$partnerListing = $element
    this.$partnerFilter = this.$partnerListing.find('.js-partner-filter')
    this.$filterItems = this.$partnerFilter.find('.js-partner-filter__item')
    this.$checkboxes = this.$partnerFilter.find('.js-partner-filter__checkbox')
    this.$panel = this.$partnerListing.first().find('.js-partner-listing__main')
    this.$mobileTrigger = this.$partnerListing.find('.js-partner-listing-mobile__trigger')
    // This should be attached at bottom of page as we have too little entries to kick server request
    // It's performance wise to keep it under the same roof
    this.partnersJSON = partnersJSON
    this.$filterItems.data('checked', false)
    this.bindEvents()
    // Elements to remove when filtering
    this.$unwanted = $('.App-listing').not(this.$partnerListing.first())
  }

  bindEvents () {
    this.$filterItems.on('click', this.handleFilterClick.bind(this))
    this.$mobileTrigger.on('click', this.handleMobileTriggerClick.bind(this))
  }

  handleFilterClick (e) {
    const $item = $(e.currentTarget)
    const $checkbox = $item.find('.js-partner-filter__checkbox')
    $item.data('checked', !$item.data('checked'))
    $checkbox.prop('checked', $item.data('checked'))
    let params = []
    this.$checkboxes.filter(':checked').each(function() {
      const $checkbox = $(this)
      const param = $checkbox.parent().text().trim()
      params.push(param)
    })
    this.filter(params)
  }

  handleMobileTriggerClick (e) {
    e.preventDefault()
    this.$partnerFilter.toggleClass('is-active')
  }

  filter (params) {
    const filteredPartners = orderBy(
        this.partnersJSON.filter(
          partner => matchPartner(partner, params)
        ), 
        partner => parseInt(partner.totalReviews), 
        'desc'
      )
    this.$unwanted.remove()
    this.$panel.html('<div class="App-listing__loader"><div class="loader"></div></div>')
    this.$partnerFilter.removeClass('is-active')
    this.insertResults(filteredPartners)

    function matchPartner (partner, params) {
      const matchSpecification = (specification, param) => specification.type == param || specification.specification == param
      const matchParam = (param, specifications) => specifications.filter(specification => matchSpecification(specification, param)).length // apply length to make the array truthy or falsy
      const paramsMatchedInSpecifications = params.filter(param => matchParam(param, partner.specifications))
      return params.length == paramsMatchedInSpecifications.length
    }
  }

  insertResults (partners) {
    const markup = partners.map(partner => this.markup(partner)).join('')
    this.$panel.html(markup)
  }

  markup (data) {
    return `
    <div class="Partner-list__row">
      <div class="Partner-list__logo-col">
        <img class="Partner-list__logo" src="${ data.partnerLogoPath }">
      </div>
      <div class="Partner-list__desc-col">
        <header class="Partner-list__header">
          <h3 class="Partner-list__title">
            <a class="Partner-list__title-link" href="${ data.url }">
             ${ data.firmName }
            </a>
          </h3>
          <div class="Partner-list__rating Rating Rating--small">
            ${ this.stars(data) }
          </div>
          <div class="Partner-list__reviews">
            (${ data.totalReviews } Reviews)
          </div>
        </header>
        <p class="Partner-list__desc">${ data.businessDescriptionShort.replace(/&quot;/g, '') }</p>
      </div>
    </div>
    `
  }

  stars (partner) {
    let output = ''
    let averageRating = parseFloat(partner.averageRating)

    if (averageRating > 0)  {
      for (let star = 1; star <= 5; star++) {
        output += `<div class="Rating__item" style="background: url('/main/img/small-star.png');">`
        if (star <= Math.floor(averageRating)) {
          output += `<div class="Rating__rate" style="clip: rect(0px,1em,1em,0px); background: url('/main/img/small-star-pale.png');"></div>`
        } else if (star == Math.ceil(averageRating)) {
          let partial_star = averageRating - Math.floor(averageRating)
          output += `<div class="Rating__rate" style="clip: rect(0px,${ partial_star }em,1em,0px); background: url('/main/img/small-star-pale.png');"></div>`
        }
        output += '</div>'
      }
    }

    return output
  }
}

new PartnerListing($('.js-partner-listing'))

function isNotCompatibleElement($element) {
  return !$element || !$element.length
}
