<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(

    '*' => array (
        'defaultWeekStartDay' => 0,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => 'auto',
        'cpTrigger' => 'admin',
        'devMode' => true,
    ),

    'warranty.jc.dsdev' => array(
        'siteUrl' => 'http://warranty.jc.dsdev',
        'environmentVariables' => array(
            'baseUrl'  => 'http://warranty.jc.dsdev/',
            'basePath' => '/var/www/warranty/',
        ),
    ),

    'warranty.bc-grp.com' => array(
		'siteUrl' => 'http://warranty.bc-grp.com/',
		'environmentVariables' => array(
			'baseUrl'  => 'http://warranty.bc-grp.com/',
			'basePath' => '/var/www/warranty/',
		),
    )
);
