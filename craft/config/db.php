<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(

    '*' => array(
		'server' => 'localhost',
		'tablePrefix' => 'craft',
	),

    'warranty.jc.dsdev' => array(
        'database' => 'warranty',
        'user' => 'root',
        'password' => 'mysql',
    ),

    'warranty.bc-grp.com' => array(
        'database' => 'warranty',
        'user' => 'warranty',
        'password' => 'dkjfeADFJ234DD',
    )
);
